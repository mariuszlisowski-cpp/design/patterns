#pragma once

#include "html_element.hpp"

#include <memory>
#include <string>

class HtmlBuilder {
public:
    HtmlBuilder(const std::string& root_name) {
        root_.setName(root_name);
    }

    static auto build(const std::string& root_name) {
        return HtmlBuilder(root_name);
        // return std::make_unique<HtmlBuilder>(root_name);
    }

    /* rigid interface */
    // void add_child(const std::string& child_name, const std::string& child_text) {
    //     root_.push_element({child_name, child_text});
    // }

    /* fluent interface */
    HtmlBuilder& add_child(const std::string& child_name, const std::string& child_text) {
        root_.push_element({child_name, child_text});
        return *this;
    }
    
    /* pointer version */
    // HtmlBuilder* add_child(const std::string& child_name, const std::string& child_text) {
    //     root_.push_element({child_name, child_text});
    //     return this;
    // }

    // operator HtmlElement() const {
    //     return root_;
    // }
    
    std::string str(int ident = 0) {
        return root_.str(ident);
    }

private:
    HtmlElement root_;
};
