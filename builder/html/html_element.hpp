#pragma once

#include <sstream>
#include <string>
#include <vector>

class HtmlElement {
public:
    HtmlElement() {}
    HtmlElement(const std::string& name, const std::string& text)
        : name_(name)
        , text_(text) {}

    std::string str(int ident = 0) {
        std::ostringstream oss;
        oss << '<' << name_ << '>' << '\n';
        for (const auto& element : elements_) {
            oss << std::string(ident, ' ')
                << '<' << element.name_ << '>'
                << element.text_
                << "</" << element.name_ << '>' << '\n';
        }
        oss << "</" << name_ << '>' << '\n';

        return oss.str();
    }

    void push_element(HtmlElement&& element) {
        elements_.emplace_back(element);
    }

    void setName(const std::string& name) {
        name_ = name;
    }
    
private:
    std::string name_;
    std::string text_;
    std::vector<HtmlElement> elements_;
};
