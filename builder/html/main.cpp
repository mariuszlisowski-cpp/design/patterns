#include <iostream>
#include <string>

#include "html_element.hpp"
#include "html_builder.hpp"

int main() {
    std::string words[] { "hello", "my", "world" };

    /* no builder */
    HtmlElement list("ul", "");
    for (const auto& word : words) {
        list.push_element({"li", word});
    }    
    std::cout << list.str(4) << std::endl;

    /* simple builder */
    HtmlBuilder builder("ul");
    for (const auto& word : words) {
        builder.add_child("li", word);
    }    
    std::cout << builder.str(4) << std::endl;

    /* fluent builder */
    builder.add_child("li", "c++").add_child("li", "g++").add_child("li", "gcc");
    std::cout << builder.str(4) << std::endl;

    /* static fluent */
    HtmlBuilder b = HtmlBuilder::build("ul")
                   .add_child("li", "one")
                   .add_child("li", "two")
                   .add_child("li", "three");
    std::cout << b.str(4) << std::endl;
    
    return 0;
}
