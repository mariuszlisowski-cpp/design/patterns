#pragma once

#include "person_builder_base.hpp"

class PersonBuilder : public PersonBuilderBase {
public:
    PersonBuilder() : PersonBuilderBase(person) {}

private:
    Person person;
};
