#include "person_job_builder.hpp"

PersonJobBuilder& PersonJobBuilder::at(const std::string& company) {
    person.company = company;
    return *this;
} 

PersonJobBuilder& PersonJobBuilder::as(const std::string& position) {
    person.position = position;
    return *this;
} 

PersonJobBuilder& PersonJobBuilder::earning(size_t income) {
    person.income = income;
    return *this;
} 
