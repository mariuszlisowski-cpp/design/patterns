#include "person_identity_builder.hpp"

PersonIdentityBuilder& PersonIdentityBuilder::called(const std::string& name) {
    person.name = name;
    return *this;
}

PersonIdentityBuilder& PersonIdentityBuilder::aged(size_t age) {
    person.age = age;
    return *this;
}
