#include "person_builder_base.hpp"

#include "person.hpp"
#include "person_address_builder.hpp"
#include "person_job_builder.hpp"
#include "person_identity_builder.hpp"

PersonIdentityBuilder PersonBuilderBase::is() const {
    return PersonIdentityBuilder(person);
}

PersonAddressBuilder PersonBuilderBase::lives() const {
    return PersonAddressBuilder(person);
}

PersonJobBuilder PersonBuilderBase::works() const {
    return PersonJobBuilder(person);
}
