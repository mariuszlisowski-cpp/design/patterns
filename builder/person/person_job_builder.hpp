#pragma once

#include "person_builder_base.hpp"

class PersonJobBuilder : public PersonBuilderBase {
public:
    explicit PersonJobBuilder(Person& person) : PersonBuilderBase(person) {}

    PersonJobBuilder& at(const std::string& company);
    PersonJobBuilder& as(const std::string& position);
    PersonJobBuilder& earning(size_t income);
};
