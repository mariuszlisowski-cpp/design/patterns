cmake_minimum_required(VERSION 3.2)
project(person)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_BUILD_TYPE Debug)

set(SOURCES
    person.cpp
    person_builder_base.cpp
    person_identity_builder.cpp
    person_address_builder.cpp
    person_job_builder.cpp
)
    
add_compile_options(-Wall -Werror -Wextra -Wconversion -pedantic)

add_library(${PROJECT_NAME}-lib STATIC ${SOURCES})
add_executable(${PROJECT_NAME} main.cpp)
target_link_libraries(${PROJECT_NAME} ${PROJECT_NAME}-lib)
