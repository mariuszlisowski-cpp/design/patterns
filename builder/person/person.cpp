#include "person.hpp"

#include "person_builder.hpp"

PersonBuilder Person::create() {
    return PersonBuilder();
}

std::ostream& operator<<(std::ostream& os, const Person& person) {
    os << "\nIs:"
       << "\n\tname: " << person.name
       << "\n\taged: " << person.age
       << "\nLives:"
       << "\n\tstreet: " << person.street
       << "\n\tcity: " << person.city
       << "\n\tpostcode: " << person.postcode
       << "\nWorks:"
       << "\n\tcity: " << person.company
       << "\n\tas: " << person.position
       << "\n\tearning: " << person.income;

    return os;
}
