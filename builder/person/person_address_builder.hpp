#pragma once

#include "person_builder_base.hpp"

class PersonAddressBuilder : public PersonBuilderBase {
public:
    explicit PersonAddressBuilder(Person& person) : PersonBuilderBase(person) {}

    PersonAddressBuilder& at(const std::string& street);
    PersonAddressBuilder& with_postcoe(const std::string& postcode);
    PersonAddressBuilder& in(const std::string& city);
};
