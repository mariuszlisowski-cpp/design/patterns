#include "person_address_builder.hpp"

PersonAddressBuilder& PersonAddressBuilder::at(const std::string& street) {
    person.street = street;
    return *this;
}

PersonAddressBuilder& PersonAddressBuilder::with_postcoe(const std::string& postcode) {
    person.postcode = postcode;
    return *this;
}

PersonAddressBuilder& PersonAddressBuilder::in(const std::string& city) {
    person.city = city;
    return *this;
}
