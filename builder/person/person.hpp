#pragma once

// #include "person_builder_base.hpp"
#include <ostream>
#include <memory>
#include <string>

class PersonBuilder;
class PersonAddressBuilder;
class PersonJobBuilder;
class PersonIdentityBuilder;

// struct Person {
class Person {
public:
    static PersonBuilder create();

private:
    Person() = default;
    std::string name;
    size_t age;

    std::string street;
    std::string postcode;
    std::string city;

    std::string company;
    std::string position;
    size_t income{};

    friend PersonBuilder;
    friend PersonAddressBuilder;
    friend PersonJobBuilder;
    friend PersonIdentityBuilder;
    friend std::ostream& operator<<(std::ostream& os, const Person& person);
};
