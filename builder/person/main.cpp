#include <iostream>

#include "person_builder.hpp"
#include "person_address_builder.hpp"
#include "person_job_builder.hpp"
#include "person_identity_builder.hpp"

int main() {
    Person employee = Person::create()
        .is()
            .called("John Smith")
            .aged(44)
        .lives()
            .at("Long Avenue")
            .in("Los Alamos")
            .with_postcoe("55447")
        .works()
            .at("BCF")
            .as("developer")
            .earning(25'000);

    std::cout << employee << std::endl;

    return 0;
}
