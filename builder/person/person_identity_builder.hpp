#pragma once

#include "person_builder_base.hpp"

class PersonIdentityBuilder : public PersonBuilderBase {
public:
    explicit PersonIdentityBuilder(Person& person) : PersonBuilderBase(person) {}

    PersonIdentityBuilder& called(const std::string& name);
    PersonIdentityBuilder& aged(size_t age);
};
