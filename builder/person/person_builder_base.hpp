#pragma once

#include <string>

#include "person.hpp"

class PersonAddressBuilder;
class PersonJobBuilder;
class PersonIdentityBuilder;

class PersonBuilderBase {
public:
    operator Person() const {
        return std::move(person);
    }
    PersonIdentityBuilder is() const;
    PersonAddressBuilder lives() const;
    PersonJobBuilder works() const;

protected:
    Person& person;
    explicit PersonBuilderBase(Person& person) : person(person) {}
};
