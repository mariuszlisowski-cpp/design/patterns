#pragma once

/* interface: quack behaviour */
class IQuack {
public:
    virtual ~IQuack() = default;
    virtual void quack() = 0;
};
