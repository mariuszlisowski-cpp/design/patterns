#pragma once

#include <iostream>

#include "pond/quack/iquack.hpp"

/* quacking definition */
class QuackLoud : public IQuack {
public:
    void quack() override;
};

/* quacking implementation */
inline void QuackLoud::quack() {
    std::cout << "quack, quack" << std::endl;        
}
