#pragma once

#include <iostream>

#include "pond/quack/iquack.hpp"

/* quacking definition */
class QuackQuiet : public IQuack {
public:
    void quack() override;
};

/* quacking implementation */
inline void QuackQuiet::quack() {
    std::cout << "! cannot quack" << std::endl;        
}
