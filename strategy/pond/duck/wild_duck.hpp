#pragma once

#include "pond/duck/duck.hpp"
#include "pond/fly/fly_with_wings.hpp"
#include "pond/quack/quack_loud.hpp"

/* derived class */
class WildDuck : public Duck {
public:
    WildDuck();
    ~WildDuck();
};

inline WildDuck::WildDuck() {
        iquack = new QuackLoud();
        ifly = new FlyWithWings();
}

inline WildDuck::~WildDuck() {
    delete iquack;
    delete ifly;
}
