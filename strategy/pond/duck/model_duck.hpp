#pragma once

#include "pond/duck/duck.hpp"
#include "pond/fly/fly_no_way.hpp"
#include "pond/quack/quack_quiet.hpp"

/* derived class */
class ModelDuck : public Duck {
public:
    ModelDuck();
    ~ModelDuck();
};

inline ModelDuck::ModelDuck() {
        iquack = new QuackQuiet();
        ifly = new FlyNoWay();
}

inline ModelDuck::~ModelDuck() {
    delete iquack;
    delete ifly;
}
