#pragma once

#include <iostream>

#include "pond/fly/ifly.hpp"
#include "pond/quack/iquack.hpp"

/* base class */
class Duck {
public:
    virtual ~Duck() = default;
    virtual void do_quack();
    virtual void do_fly();
    void swim();
    void set_fly_interface(IFly* ifly);                                     // behaviour setter method
    void set_quack_interface(IQuack* iquack);                               // saa

protected:
    IQuack* iquack;                                                         // 'has' relationship
    IFly* ifly;                                                             //  i.e. an aggregation as pointers
                                                                            // if smart pointers
                                                                            // it would be a composition (owns)
};

inline void Duck::do_quack() {
    iquack->quack();
}

inline void Duck::do_fly() {
    ifly->fly();
}

inline void Duck::swim() {
    std::cout << "swimming..." << std::endl;                                // all ducks can swim, even models
}

inline void Duck::set_fly_interface(IFly* ifly) {
    delete this->ifly;                                                      // release memory of the previous object
    this->ifly = ifly;
}

inline void Duck::set_quack_interface(IQuack* iquack) {
    delete this->iquack;                                                    // saa
    this->iquack = iquack;
}
