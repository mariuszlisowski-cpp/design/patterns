#pragma once

/* interface: fly behaviour */
class IFly {
public:
    virtual ~IFly() = default;
    virtual void fly() = 0;
};
