# pragma once

#include <iostream>

#include "pond/fly/ifly.hpp"

class FlyRocketPowered : public IFly {
public:
    void fly() override;
};

inline void FlyRocketPowered::fly() {
    std::cout << "flying rocket powered..." << std::endl;
}
