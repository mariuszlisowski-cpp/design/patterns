#pragma once

#include <iostream>

#include "pond/fly/ifly.hpp"

/* flying definition */
class FlyNoWay : public IFly {
public:
    void fly() override;
};

/* flying implementation */
inline void FlyNoWay::fly() {
        std::cout << "! cannot fly" << std::endl;
}
