#pragma once

#include <iostream>

#include "pond/fly/ifly.hpp"

/* flying definition */
class FlyWithWings : public IFly {
public:
    void fly() override;
};

/* flying implementation */
inline void FlyWithWings::fly() {
        std::cout << "flying with wings..." << std::endl;
}
