#include <iostream>

#include "duck/duck.hpp"
#include "duck/wild_duck.hpp"
#include "duck/model_duck.hpp"
#include "fly/fly_rocket_powered.hpp"

int main() {
    Duck* duck = new WildDuck();
    duck->do_quack();
    duck->do_fly();
    delete duck;

    duck = new ModelDuck();
    duck->do_quack();
    duck->do_fly();

    duck->set_fly_interface(new FlyRocketPowered());                        // dynamic behaviour change
    duck->do_fly();

    delete duck;
    duck = nullptr;

    return 0;
}
