#include "game/behaviour/run/irun.hpp"
#include "game/figure/king.hpp"
#include "game/figure/knight.hpp"
#include "game/figure/queen.hpp"
#include "game/behaviour/fight/arc.hpp"
#include "game/behaviour/run/run_fast.hpp"

void show_behaviour(Figure* figure) {
    std::cout << "# Behaviour of " << figure->getTitle() << ":\n";
    figure->fight();
    figure->run();
    figure->walk();
}

int main() {
    Figure* figure{};
    
    figure = new King();
    show_behaviour(figure);

    figure = new Knight();
    show_behaviour(figure);

    figure = new Queen();
    show_behaviour(figure);

    figure->setRun(new RunFast());
    figure->setWeapon(new Arc());
    show_behaviour(figure);

    delete figure;

    return 0;
}
