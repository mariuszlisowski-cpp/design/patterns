#pragma once

#include <string>

#include "game/behaviour/fight/iweapon.hpp"
#include "game/behaviour/run/irun.hpp"

/* base abstract class */
class Figure {
public:
    Figure(const std::string& title);
    virtual ~Figure() = default;
    virtual void fight();
    virtual void run();

    void walk();
    void setWeapon(IWeapon* iweapon);
    void setRun(IRun* irun);
    auto getTitle();

protected:
    IWeapon* iweapon;
    IRun* irun; 
    std::string title;   
};

inline Figure::Figure(const std::string& title) : title(title) {}

inline void Figure::fight() {
    iweapon->useWeapon();
}

inline void Figure::run() {
    irun->run();
}

inline void Figure::walk() {
    std::cout << "> walks the same as others" << std::endl;
}

inline void Figure::setWeapon(IWeapon* weapon) {
    delete iweapon;
    iweapon = weapon;
}

inline void Figure::setRun(IRun* run) {
    delete irun;
    irun = run;
}

inline auto Figure::getTitle() {
    return title;
}
