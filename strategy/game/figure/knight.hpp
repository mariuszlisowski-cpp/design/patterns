#pragma once

#include "game/figure/figure.hpp"
#include "game/behaviour/fight/sword.hpp"
#include "game/behaviour/run/run_fast.hpp"

class Knight : public Figure {
public:
    Knight();
    ~Knight();
};

inline Knight::Knight() : Figure("Knight")  {
    iweapon = new Sword();
    irun = new RunFast();
}

inline Knight::~Knight() {
    delete iweapon;
    delete irun;
}
