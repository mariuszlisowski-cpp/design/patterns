#pragma once

#include "game/figure/figure.hpp"
#include "game/behaviour/fight/knife.hpp"
#include "game/behaviour/run/run_slow.hpp"

class Queen : public Figure {
public:
    Queen();
    ~Queen();
};

inline Queen::Queen() : Figure("Queen")  {
    iweapon = new Knife();
    irun = new RunSlow();
}

inline Queen::~Queen() {
    delete iweapon;
    delete irun;
}
