#pragma once

#include "game/figure/figure.hpp"
#include "game/behaviour/fight/arc.hpp"
#include "game/behaviour/run/run_fast.hpp"

class King : public Figure {
public:
    King();
    ~King();
};

inline King::King() : Figure("King") {
    iweapon = new Arc;
    irun = new RunFast;
}

inline King::~King() {
    delete iweapon;
    delete irun;
}
