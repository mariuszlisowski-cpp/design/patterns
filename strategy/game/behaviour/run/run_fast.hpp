#pragma once

#include "game/behaviour/run/irun.hpp"

class RunFast : public IRun {
public:
    void run() override;
};

inline void RunFast::run() {
    std::cout << "> runs fast" << std::endl;
}
