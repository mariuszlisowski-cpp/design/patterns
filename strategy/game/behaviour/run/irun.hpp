#pragma once

#include <iostream>

class IRun {
public:
    virtual ~IRun() = default;
    virtual void run() = 0;
};
