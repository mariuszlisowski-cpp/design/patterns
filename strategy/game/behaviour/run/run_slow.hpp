#pragma once

#include "game/behaviour/run/irun.hpp"

class RunSlow : public IRun {
public:
    void run() override;
};

inline void RunSlow::run() {
    std::cout << "> runs slow" << std::endl;
}
