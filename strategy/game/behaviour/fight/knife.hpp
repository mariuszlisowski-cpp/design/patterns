#pragma once

#include "game/behaviour/fight/iweapon.hpp"

class Knife : public IWeapon {
public:
    void useWeapon() override;
};

inline void Knife::useWeapon() {
    std::cout << "> uses a knife" << std::endl;
}
