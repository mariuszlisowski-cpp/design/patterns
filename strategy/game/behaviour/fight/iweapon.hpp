#pragma once

#include <iostream>

class IWeapon {
public:
    virtual ~IWeapon() = default;
    virtual void useWeapon() = 0;
};
