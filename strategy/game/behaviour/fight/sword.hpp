#pragma once

#include "game/behaviour/fight/iweapon.hpp"

class Sword : public IWeapon {
public:
    void useWeapon() override;
};

inline void Sword::useWeapon() {
    std::cout << "> uses a sword" << std::endl;
}
