#pragma once

#include "game/behaviour/fight/iweapon.hpp"

class Arc : public IWeapon {
public:
    void useWeapon() override;
};

inline void Arc::useWeapon() {
    std::cout << "> uses an arc" << std::endl;
}
